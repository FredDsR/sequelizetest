const express = require('express')

require('./database')

const app = express()

app.use(express.json())
app.use(require('./routes'))

app.listen(3333)
console.log('Server running...')